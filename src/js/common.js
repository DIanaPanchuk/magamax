var ta;
$( function() {
	autosize($('textarea'));

	$( "select.form-control" ).selectmenu({
		select: function( event, ui ) {
			$(this).next('.ui-selectmenu-button').addClass('selected');
		}
	});

	$('.js-accordion').on('shown.bs.collapse', function (e) {
		var headingPos = $(e.target).parent().find('.js-accordion-heading');
		if (headingPos.length) {
			$('html,body').animate({ scrollTop: headingPos.offset().top }, 1000);
		}
	});

	var fileInputs = $('.js-btn-file input[type=\"file\"]');
	if (fileInputs.length > 1) {
		$.each(fileInputs, function(i, el) {
			toggleFileBtn($(el));
		});
	}

	initFileInputs($('.js-btn-file'));

	$('.js-add-row').on('click', function() {
		addTableRow($(this).data('table'));
	});
});
function initFileInputs(target) {
	$(target).find('.btn').on('click', function() {
		$(this).parent().find('input[type=\"file\"]').trigger('click');
	});
	$(target).find('input[type=\"file\"]').on('change', function() {
		toggleFileBtn($(this));
	});
}

function toggleFileBtn(el) {
	if (el.val().length) {
		$(el).parent().find('.btn').removeClass('i-clip').addClass('i-ok');
	}
}
function addTableRow(table) {
	var template;
	switch(table) {
		case 'claim-quantity':
			template = '<tr class="new-row">' +
						'<td class=\"sku\"><input class=\"form-control\" value=\"\" type=\"text\"></td>' +
						'<td class=\"name\"><span class=\"hint\">Наименование автоматически заполнится по артикулу</span></td>' +
						'<td class=\"q-docs\"><input class=\"form-control\" value=\"\" type=\"text\"></td>' +
						'<td class=\"q-fact\"><input class=\"form-control\" value=\"\" type=\"text\"></td>' +
						'<td class=\"diff\"><input class=\"form-control\" value=\"\" type=\"text\"></td>' +
					'</tr>';
			break;
		case'claim-quality':
			template = '<tr class="new-row">'+
						'<td class=\"sku\"><input class=\"form-control\" value=\"\" type=\"text\"></td>' +
						'<td class=\"name\"><span class=\"hint">Наименование автоматически заполнится по артикулу</span></td>' +
						'<td class=\"quantity\"><input class=\"form-control\" value=\"\" type=\"text\"></td>' +
						'<td class=\"desc\"><textarea class=\"form-control\"></textarea></td>' +
						'<td class=\"photo\">' +
							'<div class=\"form-group file js-btn-file\">' +
								'<input name=\"file\" type=\"file\">' +
								'<button type=\"button\" class=\"btn btn-info btn-icon i-clip\">Фото</button>' +
							'</div>' +
						'</td>' +
					'</tr>';
			break;
	}
	$('.' + table + '-table').find('table').append(template);

	// init textarea autosize for new rows
	autosize($('.new-row textarea'));

	//init file upload action
	initFileInputs($('.new-row .js-btn-file'));

	$('tr').removeClass('new-row');
}
