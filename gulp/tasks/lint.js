var gulp = require('gulp');
var eslint = require('gulp-eslint');
var config = require('../config');
var notify = require('gulp-notify');
var browserSync = require('browser-sync');
var reload = browserSync.reload;

gulp.task('jslint', function () {
	gulp.src([config.src.js+'**/*.js'])
		.pipe(eslint())
		.pipe(eslint.format('codeframe'));
});

gulp.task('jslint:watch', function() {
	gulp.watch(config.src.js+'*', ['jslint']);
});
