var gulp = require('gulp');
var include = require("gulp-include");
var babel = require('gulp-babel');
var config = require('../config');
var notify = require('gulp-notify');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var compress = require('gulp-yuicompressor');
var strip = require('gulp-strip-comments');

gulp.task('js', function () {
	gulp.src(config.src.js+'**/*.js')
		.pipe(include())
		.on('error', function() {
			notify("Javascript include error");
		})
		// .pipe(babel({
		// 	presets: ['es2015']
		// }))
		.pipe(gulp.dest(config.dest.js))
		.pipe(reload({stream: true}));
});

gulp.task('js-minified', function () {
	gulp.src(config.src.js+'**/*.js')
		.pipe(include())
		.pipe(compress({type: 'js'}))
		.on('error', function() {
			notify("Javascript include error");
		})
		.pipe(strip())
		.pipe(gulp.dest(config.dest.js))
		.pipe(reload({stream: true}));
});

gulp.task('js:watch', function() {
	gulp.watch(config.src.js+'*', ['js']);
});
