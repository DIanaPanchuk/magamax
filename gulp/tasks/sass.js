var gulp = require('gulp');
var postcss = require('gulp-postcss');
var sass = require('gulp-ruby-sass');
var scsslint = require('gulp-scss-lint');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
var notify = require('gulp-notify');
var mqpacker = require("css-mqpacker");
var config = require('../config');
var fs = require('fs');
// var orderedMergeStream = require('ordered-merge-stream');

gulp.task('sass', function() {

	var processors = [
		autoprefixer({browsers: ['last 15 versions'], cascade: false}),
		mqpacker()
	];

	return sass(config.src.sass+'*.scss', {
		sourcemap: true,
		style: 'compact',
		emitCompileError: true
	})
	.on('error', notify.onError({
		title: 'Sass Error!',
		message: '<%= error.message %>'
	}))
	.pipe(postcss(processors))
	.pipe(sourcemaps.write('./'))
	.pipe(gulp.dest(config.dest.css));

	// var sassStream = sass(config.src.sass+'*.scss', {
	// 		sourcemap: true,
	// 		style: 'compact',
	// 		emitCompileError: true
	// 	});
	//
	// var cssStream = gulp.src(config.src.root + 'lib/bootstrap/dist/css/bootstrap.css');
	//
	// return orderedMergeStream([cssStream, sassStream])
	// 	.pipe(postcss(processors))
	// 	.pipe(sourcemaps.write('./'))
	// 	.pipe(gulp.dest(config.dest.css));
});

gulp.task('sass-minified', function() {

	var processors = [
		autoprefixer({browsers: ['last 15 versions'], cascade: false}),
		mqpacker()
	];

	return sass(config.src.sass+'*.scss', {
		sourcemap: true,
		style: 'compressed',
		emitCompileError: true
	})
	.on('error', notify.onError({
		title: 'Sass Error!',
		message: '<%= error.message %>'
	}))
	.pipe(postcss(processors))
	.pipe(gulp.dest(config.dest.css));

});

gulp.task('scss-lint', function() {
	return gulp.src([config.src.sass + '**/*.scss',
			'!' + config.src.sass + '_bootstrap-config.scss',
			'!' + config.src.sass + 'lib/*.*'])
		.pipe(scsslint({
			options: {
				formatter: 'stylish',
				'merge-default-rules': false,
			},
			config: '.scss-lint.yml'
		}));
});

gulp.task('sass:watch', function() {
	gulp.watch(config.src.sass + '/**/*', ['sass']);
});
